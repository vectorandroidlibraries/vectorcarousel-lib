package com.egmdevelopers.vectorcarousel

import android.util.SparseArray
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView

/**
 *
 * @author Ernesto Galvez Martinez
 */
abstract class CarouselAdapter<VH : RecyclerView.ViewHolder> : RecyclerView.Adapter<VH>() {

    // =============================================================================================
    //     PROPERTIES
    // =============================================================================================
    internal val viewHolders = SparseArray<VH>()


    // =============================================================================================
    //     RECYCLER VIEW
    // =============================================================================================
    @CallSuper
    override fun onBindViewHolder(holder: VH, position: Int) {
        bindVH(holder, position)
        viewHolders.put(position, holder)
    }


    // =============================================================================================
    //     BINDERS
    // =============================================================================================

    /**
     * This method should update the contents of the {@link VH#itemView} to reflect
     * the item at the given position.
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *               item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     *
     * - We can´t create an object from an Abstract class
     * - Internal properties are by default non-abstract
     * - If we want to override this members in child we have to use "open"
     * - abstract are "open" by default
     * - abstract fun doesn't have a body and it must be implemented in the derived class
     */
    abstract fun bindVH(holder: VH, position: Int)

}
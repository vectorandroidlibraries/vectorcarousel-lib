package com.egmdevelopers.carouseldemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.egmdevelopers.vectorcarousel.CarouselAdapter
import com.github.islamkhsh.CardSliderAdapter


/**
 * GenericCardAdapter
 * @author: Ernesto Gálvez Martínez
 */
class GenericCardAdapter : CarouselAdapter<GenericCardAdapter.GenericCardViewHolder>() {
//class GenericCardAdapter : CardSliderAdapter<GenericCardAdapter.GenericCardViewHolder>() {

    override fun getItemCount(): Int {
        return 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericCardViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.generic_card, parent, false)
        return GenericCardViewHolder(view)
    }

    override fun bindVH(holder: GenericCardViewHolder, position: Int) {

    }


    class GenericCardViewHolder(view: View) : RecyclerView.ViewHolder(view)

}
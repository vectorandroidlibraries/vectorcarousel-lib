package com.egmdevelopers.carouseldemo

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.egmdevelopers.carouseldemo.databinding.ActivityMainBinding
import com.egmdevelopers.vectorcarousel.CarouselViewPager
import com.egmdevelopers.vectorcarousel.viewpager2.ViewPager2
import com.github.islamkhsh.CardSliderViewPager

class MainActivity : AppCompatActivity() {

    // =============================================================================================
    //     PROPERTIES
    // =============================================================================================
    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView(this, R.layout.activity_main)
    }


    // =============================================================================================
    //     ACTIVITY LIFECYCLE
    // =============================================================================================
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.apply {
            setSupportActionBar(toolbar)
            include.viewPager.apply {
                adapter = GenericCardAdapter()
                registerOnPageChangeCallback(pagerCallback)
            }
            fab.setOnClickListener { view ->
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }
    }

    // =============================================================================================
    //     CALLBACKS
    // =============================================================================================
    private val pagerCallback = object: ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            Toast.makeText(this@MainActivity, "Tarjeta $position seleccionada", Toast.LENGTH_SHORT).show()
        }
    }



    // =============================================================================================
    //     MENU
    // =============================================================================================
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


}